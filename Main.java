/* The following java program demonstrates how to compare the state of two objects without using comparator*/


//class is created
class Com
{
	public  String name;
	public  int age;

Com()
{
}
//constructor of the class Com is created
Com(String name,int age)
{
	this.name=name;
	this.age=age;
}

//Overridden equals method
@Override
public  boolean equals(Object A)
{
	Com b=(Com)A;
	return this.name.equals(b.name) && this.age==b.age;		
}

//Overridden hashCode method
@Override
public int hashCode()
{
	return age+name.hashCode();
}
